package Model;

import java.util.ArrayList;

public class Book {
	private String bookName;
	private String author;
	private int page;
	private ArrayList<String> book = new ArrayList<String>();
	
	public Book(String bookname,int pages,String author){
		this.bookName = bookname;
		this.page = pages;
		this.author = author;
	}
	
	public String getBookName(){
		return bookName;
	}
}

