package Model;

public class Student {
	private String firstName;
	private String lastName;
	private int date;
	
	public Student(String name,String lastname,String id){
		this.firstName = name;
		this.lastName = lastname;
	}
	
	public String getBorrowedBook(Book b){
		return b.getBookName();
	}
	
	public String getBorrowedBook(ReferencesBook r){
		return r.getBookName();
	}
	
	public String getName(){
		return firstName + " " + lastName;
	}
	
	
}
