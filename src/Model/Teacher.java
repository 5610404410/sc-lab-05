package Model;

public class Teacher {
	private String firstName;
	private String lastName;
	
	public Teacher(String name,String lastname){
		this.firstName = name;
		this.lastName = lastname;
	}
	
	public String getBorrowedBook(Book b){
		return b.getBookName();
	}
	
	public String getBorrowedBook(ReferencesBook r){
		return r.getBookName();
	}
	
	public String getName(){
		return firstName + " " + lastName;
	}
}
