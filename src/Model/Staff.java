package Model;

public class Staff {
	private String firstName;
	private String lastName;
	
	public Staff(String name,String lastname){
		this.firstName = name;
		this.lastName = lastname;
	}
	
	public String getBorrowedBook(Book b){
		return b.getBookName();
	}
	
	public String getBorrowedBook(ReferencesBook r){
		return r.getBookName();
	}
	
	public String getName(){
		return firstName + " " + lastName;
	}
}
