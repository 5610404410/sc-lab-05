package Model;

import java.util.ArrayList;

public class Library {
	private int count;
	private ArrayList<String> bookSet = new ArrayList<String>();
	private ArrayList<String> refBookSet = new ArrayList<String>();
	private int dateBorrow;
	private int dateReturn;
	
	public void addBook(Book book){
		count++;
		bookSet.add(book.getBookName());
	}
	
	public void addRefBook(ReferencesBook refbook){
		count++;
		refBookSet.add(refbook.getBookName());
	}
	
	public int bookCount(){
		return count;
	}
	
	public String borrowBook(Student s,String bookname){

		if ((bookSet.contains(bookname) == true) || (refBookSet.contains(bookname) == true)){
			count--;
			System.out.println(bookname + " is in library.");
			bookSet.remove(bookname);
			System.out.println("You have borrowed.");
		}
		else if ((bookSet.contains(bookname) == false) || (refBookSet.contains(bookname) == false)){
			System.out.println(bookname + " not in library.");
		}
		return bookname;
	}	
	
	public void borrowBook(Teacher t,String bookname){
		if ((bookSet.contains(bookname) == true) || (refBookSet.contains(bookname) == true)){
			count--;			
			System.out.println(bookname + " is in library.");
			bookSet.remove(bookname);
			System.out.println("You have borrowed.");
		}
		else if ((bookSet.contains(bookname) == false) || (refBookSet.contains(bookname) == false)){
			System.out.println(bookname + " not in library.");
		}
	}	
	public void borrowBook(Staff f,String bookname){
		if ((bookSet.contains(bookname) == true) || (refBookSet.contains(bookname) == true)){
			count--;
			System.out.println(bookname + " is in library.");
			bookSet.remove(bookname);
			System.out.println("You have borrowed.");
		}
		else if ((bookSet.contains(bookname) == false) || (refBookSet.contains(bookname) == false)){
			System.out.println(bookname + " not in library.");
		}
	}
	public void returnBook(Student s,String bookname){
		bookSet.add(bookname);
		count++;
		System.out.println("You have return " + bookname);
	}
	public void returnBook(Teacher t,String bookname){
		bookSet.add(bookname);
		count++;
		System.out.println("You have return " + bookname);
	}
	public void returnBook(Staff f,String bookname){
		bookSet.add(bookname);
		count++;
		System.out.println("You have return " + bookname);
	}
	
	public void checkDateStudent(int dateBorrow,int DateReturn){
		if(dateReturn - dateBorrow > 7){
			System.out.println("You must pay money.");
		}
	}
	public String getBorrower(Student s){
		return s.getName();
	}
	public String getBorrower(Teacher t){
		return t.getName();
	}
	public String getBorrower(Staff f){
		return f.getName();
	}
}
