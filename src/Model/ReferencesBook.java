package Model;

public class ReferencesBook {
	private String bookName;
	private String author;
	private int page;
	
	public ReferencesBook(String bookname,int pages,String author){
		this.author = author;
		this.bookName = bookname;
		this.page = pages;
	}
	
	public String getBookName(){
		return bookName;
	}
}
