package Console;

import Model.Book;
import Model.Library;
import Model.ReferencesBook;
import Model.Staff;
import Model.Student;
import Model.Teacher;

public class Console {
	private Library lib;
	private Student stu;
	private Teacher teach;
	private Staff staff;
	private Book book1;
	private Book book2;
	private ReferencesBook refbook;
	
	public Console(){
		testCase();
	}
	public void testCase(){	
		lib = new Library();
		stu = new Student("Thanisa","Limthammaros","5610404410");
		teach = new Teacher("Usa","Sammapan");
		staff = new Staff("Pimnipa","Pitaktikul");
		book1 = new Book ("Big Java",400,"Watson");
		book2 = new Book ("PPL",500,"Ajarn");
		refbook = new ReferencesBook ("Dictionary",600,"Kasetsart University");
		
		lib.addBook(book1);
		lib.addBook(book2);
		lib.addRefBook(refbook);
		
		System.out.println("In Library have books " + lib.bookCount() + " books.");
		System.out.println();
		
		System.out.println("Your name: " + lib.getBorrower(stu));
		lib.borrowBook(stu, "Big Java");
		System.out.println();
		
		System.out.println("Your name: " + lib.getBorrower(staff));
		lib.borrowBook(staff, "Dictionary");
		System.out.println();
		
		System.out.println("Your name: " + lib.getBorrower(teach));
		lib.borrowBook(teach, "666");		
		System.out.println();
		
		System.out.println("In Library have books " + lib.bookCount() + " books.");
		System.out.println();
		
		System.out.println("Your name: " + lib.getBorrower(stu));
		lib.returnBook(stu, "Big Java");
		lib.checkDateStudent(3,15);
		System.out.println();
		
		System.out.println("In Library have books " + lib.bookCount() + " books.");
		System.out.println();
		
	}
 
}
